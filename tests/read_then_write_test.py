# Testing that reading a file with read_file, and then writing it again,
# produces the same result.
# To make sure that a difference is not due to legitimate formatting
# difference use a file that was produced by pybtex.

import glob
import os
import unittest

from pybtex.database import parse_file, parse_string


class ReadThenWriteTest(unittest.TestCase):

    def setUp(self):
        self.maxDiff = None

    def test_read_then_write_produces_same_output(self):
        bibs_glob = os.path.join('tests', 'data', '*.bib')
        for filename in glob.glob(bibs_glob):
            database = parse_file(filename)
            output_a = database.to_string(bib_format='bibtex')
            database = parse_string(output_a, bib_format='bibtex')
            output_b = database.to_string(bib_format='bibtex')
            self.assertEqual(output_a, output_b)
